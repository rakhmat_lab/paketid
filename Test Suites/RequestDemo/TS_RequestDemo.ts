<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>TS_RequestDemo</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>ce8c75db-23fd-4a0f-83b3-938209334466</testSuiteGuid>
   <testCaseLink>
      <guid>0325ca34-8490-4569-9881-199155d90ea8</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/requestDemo/RequestDemo_001</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d43a1cd7-f4f1-41f6-a5e4-ea2df3113989</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/requestDemo/RequestDemo_002</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>3f60f768-0639-4006-8ade-49f35d9916c5</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/requestDemo/RequestDemo_003</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ce8e7e55-41bf-4ac0-80ba-f35d8e787659</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/requestDemo/RequestDemo_004</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>bf9e9f50-c9ff-4ce7-a013-9887dea7a705</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/requestDemo/RequestDemo_005</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
