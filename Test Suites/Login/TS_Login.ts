<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>TS_Login</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>599e8858-4518-4410-8451-587b619849b9</testSuiteGuid>
   <testCaseLink>
      <guid>97adbbfb-f9a7-4182-823d-786f9d04a16e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Login/Login-001</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>9ec3ea73-3954-4b10-976c-66700172f165</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Login/Login_004</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>608ddff9-cd74-4c7b-b69b-1b881cf131f3</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Login/Login_005</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a2adacd3-47ce-48c4-80a4-064e2160d542</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Login/Login_006</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b86eebe8-7e1d-49a4-b263-829dc3ae1443</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Login/Login_007</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>2088329f-2abc-4cb0-b700-59cf32ae04a0</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Login/Login_008</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
