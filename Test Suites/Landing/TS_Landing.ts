<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>TS_Landing</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>04d197dd-4e13-453d-8aa0-afc2a1f73809</testSuiteGuid>
   <testCaseLink>
      <guid>aa15f979-34a2-4bb5-96bc-3b88371e787c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/landingPage/Landing_001</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a83ed106-57ce-4393-8070-abdd8d37e1d6</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/landingPage/Landing_002</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c05a6581-28d3-4ead-a735-f6701ca39d72</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/landingPage/Landing_003</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>40c188f7-adf9-4181-8ca1-83828dfd81b0</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/landingPage/Landing_004</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>3418aea4-68c3-430a-8cb4-371725a2dab6</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/landingPage/Landing_005</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>dbaf155d-aa53-4653-afa5-ece4e32a6901</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/landingPage/Landing_006</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>9b4815a6-87bd-4741-965a-603de8541a3c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/landingPage/Landing_007</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>164ae21f-6612-4a02-8d19-0a94fa2c090c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/landingPage/Landing_008</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>0d0bf4c5-a22a-4f44-912e-c0e67dd34083</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/landingPage/Landing_009</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>4360e22e-e812-4512-9f4a-544c3bcba77b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/landingPage/Landing_016</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d6c0b519-b51f-4e1e-b063-68a0925c9e95</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/landingPage/Landing_017</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>8f90fd8b-3e75-40c5-b825-873731a6018b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/landingPage/Landing_018</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>9b2ecc55-c1df-44c8-8ac2-705074688ec0</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/landingPage/Landing_019</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>7d1ea837-8e6d-4d8d-a9c1-276ab4d3ac08</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/landingPage/Landing_020</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>9b6bc12f-5d86-409f-b734-5eb228de58f6</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/landingPage/Landing_021</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a9d32784-7d6f-4f23-8277-6af5dc2c1cc1</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/landingPage/Landing_022</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a61be146-caee-48d9-8731-a97d91b03e9c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/landingPage/Landing_023</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
